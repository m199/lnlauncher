#ifndef ICON_VIEW_H
#define ICON_VIEW_H

#include <View.h>
#include <String.h>
#include <Bitmap.h>
#include <Mime.h>
#include <Path.h>

class IconView : public BView
{
	public:
		IconView( entry_ref * );
		IconView( BMessage * ); // un-archive
		virtual ~IconView();
		
		static BArchivable * Instantiate( BMessage * );
		virtual status_t	Archive( BMessage *, bool ) const;

		virtual void DetachedFromWindow();

		virtual void Draw( BRect );
		
		virtual void MouseDown( BPoint );
		virtual void MouseMoved( BPoint, uint32, const BMessage* );
		virtual void MouseUp( BPoint );
		
		virtual void MessageReceived( BMessage * );
		
		void Run( const char * args = NULL );
		
		void Update( bool get_icons = true ); // gets icons, mime type, actual path if needed
		void OpenParent();

	protected:
		BPath		m_path, m_actual_path;
		BMimeType	m_app_sig;
		BBitmap		* m_large_icon, * m_small_icon;
		
		bool		m_mouse_down;
		bool		m_moving;
		int32		m_buttons;
		
		BString		m_args_string;
};

#endif

#include "HandleView.h"

#include "RosterPrivate.h"

#include <Application.h>
#include <cstdio>
#include <Screen.h>
#include <MenuItem.h>
#include <PopUpMenu.h>
#include <Alert.h>
#include <Dragger.h>
#include <Roster.h>
#include <Message.h>


#include <Catalog.h>
#include <Locale.h>

#include "PanelWindow.h"

#undef B_TRANSLATION_CONTEXT
#define B_TRANSLATION_CONTEXT "LnLauncherMainMenu"

static const char* kSure = B_TRANSLATE_MARK("Are you sure you want to remove this panel?"); 
static const char* kIcoSize = B_TRANSLATE_MARK("Size");
static const char* kLargeIco = B_TRANSLATE_MARK("Large");
static const char* kSmallIco = B_TRANSLATE_MARK("Small");
static const char* kThickness = B_TRANSLATE_MARK("Thickness");
static const char* kThickNormal = B_TRANSLATE_MARK("Normal");
static const char* kThickThin = B_TRANSLATE_MARK("Thin");
static const char* kScrollspeed = B_TRANSLATE_MARK("Scrollspeed");
static const char* kSpeedSlow = B_TRANSLATE_MARK("Slow");
static const char* kSpeedNormal = B_TRANSLATE_MARK("Normal");
static const char* kSpeedFast = B_TRANSLATE_MARK("Fast");
static const char* kSpeedInstant = B_TRANSLATE_MARK("Instant");
static const char* kExpOnMouseOver = B_TRANSLATE_MARK("Expand on mouse-over");
static const char* kOn = B_TRANSLATE_MARK("on");
static const char* kYes = B_TRANSLATE_MARK("yes");
static const char* kNo = B_TRANSLATE_MARK("No");
static const char* kOff = B_TRANSLATE_MARK("off");
static const char* kFloating = B_TRANSLATE_MARK("Floating");
static const char* kPanel = B_TRANSLATE_MARK("Panel");
static const char* kPanelNew = B_TRANSLATE_MARK("New");
static const char* kPanelRemove = B_TRANSLATE_MARK("Remove This");
static const char* kReplicants = B_TRANSLATE_MARK("Replicants");
static const char* kRepShow = B_TRANSLATE_MARK("Show");
static const char* kRepHide = B_TRANSLATE_MARK("Hide");
static const char* kStartTerminal = B_TRANSLATE_MARK("Terminal");
static const char* kShutdown = B_TRANSLATE_MARK("Shutdown");
static const char* kShutdownReboot = B_TRANSLATE_MARK("Reboot");
static const char* kShutdownPoweroff = B_TRANSLATE_MARK("Poweroff");
static const char* kSuspend = B_TRANSLATE_MARK("Suspend");
static const char* kQuit = B_TRANSLATE_MARK("Quit");

// from private_roster.h (or something like that, stolen from Deskbar src :)
const uint32 kMsgShutdownSystem = 301;
const uint32 kMsgRebootSystem = 302;
const uint32 kMsgSuspendSystem = 304;

HandleView::HandleView()
:	BView(
		BRect(0,0,10,35),
		"handle",
		B_FOLLOW_NONE,
		B_WILL_DRAW|B_PULSE_NEEDED|B_FULL_UPDATE_ON_RESIZE
	),
	m_mouse_down( false ),
	m_moving_panel( false ),
	fMenu(NULL)
{
	SetViewColor( 255,203,0,0 );
}

HandleView::HandleView( BMessage * msg )
:	BView( msg ),
	m_mouse_down( false ),
	m_moving_panel( false ),
	fMenu(NULL)
{
	SetFlags( Flags() | B_FULL_UPDATE_ON_RESIZE );
}

HandleView::~HandleView()
{
	delete fMenu;
}

BArchivable *
HandleView::Instantiate( BMessage * msg )
{
	if ( !validate_instantiation( msg, "HandleView" ) )
		return NULL;
	return new HandleView(msg);
}

status_t
HandleView::Archive( BMessage * msg, bool deep ) const
{
	return BView::Archive(msg,deep);
}
/*
void
HandleView::Pulse()
{
	Window()->PostMessage( PANEL_MODIFY_SIZE );
}
*/
void
HandleView::MouseDown( BPoint pos )
{
	SetMouseEventMask( B_POINTER_EVENTS, B_LOCK_WINDOW_FOCUS );
	Window()->CurrentMessage()->FindInt32("buttons",&m_buttons);
	
	m_mouse_down = true;
	
	Invalidate();
}

void
HandleView::MouseUp( BPoint pos )
{
	if ( !m_mouse_down )
		return;
	
	if ( m_buttons & B_SECONDARY_MOUSE_BUTTON )
	{
		if ( !m_moving_panel )
		{ // show pop-up
			ShowPopup( ConvertToScreen( pos ) );
		}
	} else
	{ // primary mouse button
		if ( Bounds().Contains( pos ) )
			Window()->PostMessage( PANEL_TOGGLE_OPEN );
	}
	
	m_mouse_down = false;
	m_moving_panel = false;

	Invalidate();
}

void
HandleView::MouseMoved( BPoint point, uint32 transit, const BMessage * )
{
	// for 'expand on mouse over'
	if ( transit == B_ENTERED_VIEW && !m_mouse_down )
	{ // enter view
		Window()->PostMessage( PANEL_MOUSE_ENTER );
	}
	
	if ( ( transit == B_EXITED_VIEW || transit == B_OUTSIDE_VIEW ) && !m_mouse_down )
	{ // exit view (and window)
		if ( !Window()->Frame().Contains( ConvertToScreen(point) ) )
			Window()->PostMessage( PANEL_MOUSE_EXIT );
	}
	
	int32 buttons = 0;
	Window()->CurrentMessage()->FindInt32("buttons",&buttons);
	
	if ( m_mouse_down && (buttons & B_SECONDARY_MOUSE_BUTTON) )
	{ // secondary mouse button pressed
		BPoint scr_pos = ConvertToScreen(point);
		if ( !Window()->Frame().Contains( scr_pos ) || m_moving_panel )
		{ // move window
			m_moving_panel = true;
			
			BScreen screen( Window() );
			
			const float CORNER = 10;
			
			if ( scr_pos.y == 0 )
			{
				if ( scr_pos.x > CORNER && scr_pos.x < (screen.Frame().right - CORNER ) )
				{
					BMessage msg( PANEL_SET_EDGE );
					msg.AddInt8( "edge", PanelWindow::TOP );
					Window()->PostMessage( &msg );
				}
			}
			if ( scr_pos.x == 0 )
			{
				if ( scr_pos.y > CORNER && scr_pos.y < (screen.Frame().bottom - CORNER ) )
				{
					BMessage msg( PANEL_SET_EDGE );
					msg.AddInt8( "edge", PanelWindow::LEFT );
					Window()->PostMessage( &msg );
				}
			}
			if ( scr_pos.x == screen.Frame().right )
			{
				if ( scr_pos.y > CORNER && scr_pos.y < (screen.Frame().bottom - CORNER ) )
				{
					BMessage msg( PANEL_SET_EDGE );
					msg.AddInt8( "edge", PanelWindow::RIGHT );
					Window()->PostMessage( &msg );
				}
			}
			if ( scr_pos.y == screen.Frame().bottom )
			{
				if ( scr_pos.x > CORNER && scr_pos.x < (screen.Frame().right - CORNER ) )
				{
					BMessage msg( PANEL_SET_EDGE );
					msg.AddInt8( "edge", PanelWindow::BOTTOM );
					Window()->PostMessage( &msg );
				}
			}
			
			BMessage msg(PANEL_MOVE_TO );
			msg.AddPoint( "pos", scr_pos );
			Window()->PostMessage( &msg );
		}
	}
}

void
HandleView::Draw( BRect )
{
	rgb_color light  = ViewColor();
	rgb_color bright = ViewColor();
	rgb_color dark   = ViewColor();
	
	// set up colors
	if ( bright.red < 175 )   bright.red += 80;   else bright.red = 255;
	if ( bright.green < 175 ) bright.green += 80; else bright.green = 255;
	if ( bright.blue < 175 )  bright.blue += 80;  else bright.blue = 255;
	
	if ( dark.red > 79 )   dark.red -= 80;   else dark.red = 0;
	if ( dark.green > 79 ) dark.green -= 80; else dark.green = 0;
	if ( dark.blue > 79 )  dark.blue -= 80;  else dark.blue = 0;
	
	if ( m_mouse_down )
	{
		light	= tint_color( light, B_HIGHLIGHT_BACKGROUND_TINT );
		bright	= tint_color( bright, B_HIGHLIGHT_BACKGROUND_TINT );
		dark	= tint_color( dark, B_HIGHLIGHT_BACKGROUND_TINT );
	}
	
	BRect bounds = Bounds();
	
	// border
	MovePenTo( bounds.RightTop() );
	
	SetHighColor( bright );		
	StrokeLine( bounds.LeftTop() );
	StrokeLine( bounds.LeftBottom() );
	
	SetHighColor( dark );
	StrokeLine( bounds.RightBottom() );
	StrokeLine( bounds.RightTop() );
	
	// interior
	BRect temp = bounds; temp.InsetBy(1,1);
	SetHighColor( light );		FillRect( temp );
	
	float minx = 3;
	float maxx = bounds.right-2;
	float miny = 2;
	float maxy = bounds.bottom-1;
	
	if ( bounds.Width() > bounds.Height() )
	{
		minx = 2;
		maxx = bounds.right-1;
		miny = 3;
		maxy = bounds.bottom-2;
	}
	
	// grip-friendly surface look
	SetHighColor( bright );
	for ( float x=minx; x<maxx; x+=3 )
	{
		for ( float y=miny; y<maxy; y+=3 )
		{
			BPoint pos(x,y);
			StrokeLine( pos,pos );
		}
	}

	SetHighColor( dark );
	for ( float x=minx+1; x<maxx; x+=3 )
	{
		for ( float y=miny+1; y<maxy; y+=3 )
		{
			BPoint pos(x,y);
			StrokeLine( pos,pos );
		}
	}
}


void
HandleView::MessageReceived( BMessage * msg )
{
	switch ( msg->what )
	{
		case 'Sfly':
		{ // set floating on
			BMessage win_msg( PANEL_SET_FLOATING );
			win_msg.AddBool("floating_window",true);
			Window()->PostMessage( &win_msg );
		}	break;
		case 'Sfln':
		{ // set floating off
			BMessage win_msg( PANEL_SET_FLOATING );
			win_msg.AddBool("floating_window",false);
			Window()->PostMessage( &win_msg );
		}	break;

		case 'THno':
		{ // set not thin handle
			BMessage win_msg( PANEL_SET_THIN_HANDLE );
			win_msg.AddBool("thin_handle",false);
			Window()->PostMessage( &win_msg );
		}	break;
		case 'THth':
		{ // set thin handle
			BMessage win_msg( PANEL_SET_THIN_HANDLE );
			win_msg.AddBool("thin_handle",true);
			Window()->PostMessage( &win_msg );
		}	break;

		case 'Sico':
		{ // set small icons
			BMessage win_msg( PANEL_SET_ICON_SIZE );
			win_msg.AddBool("small_icons",true);
			Window()->PostMessage( &win_msg );
		}	break;
		case 'Lico':
		{ // set large icons
			BMessage win_msg( PANEL_SET_ICON_SIZE );
			win_msg.AddBool("small_icons",false);
			Window()->PostMessage( &win_msg );
		}	break;

		case 'DATA':
		{ // file drop
			msg->what = PANEL_ADD_ICON;
			Window()->PostMessage(msg);
		}	break;
		case 'Quit':
		{
			be_app->PostMessage( B_QUIT_REQUESTED );
		}
			break;
		case 'Auto':
		{
			fAutoStartMenu->SetMarked(!fAutoStartMenu->IsMarked());
			BMessage msg('Auto');
			msg.AddBool("autostart", fAutoStartMenu->IsMarked());
			be_app->PostMessage(&msg);
		}
			break;
		case 'PSTE':
		{ // color drop
			rgb_color * col;
			ssize_t sz;
			if (msg->FindData("RGBColor",B_RGB_COLOR_TYPE, (const void**)&col, &sz) == B_OK)
			{
				SetViewColor( *col );
				Invalidate();
			}
		}
			break;
		case 'Npan':
		{ // new panel
			be_app->PostMessage( msg );
		}	break;
		case 'Rpan':
		{ // remove panel
			BAlert * alert = new BAlert(
				"title",
				B_TRANSLATE(kSure),
				B_TRANSLATE(kYes),
				B_TRANSLATE(kNo)
			);
			
			if ( alert->Go() != 0 )
				break;
			
			msg->AddPointer("panel", Window());
			be_app->PostMessage( msg );
		}	break;
		case 'SCsl':
		{ // Set scroll rate, SLOW
			BMessage win_msg( PANEL_SET_SCROLL_RATE );
			win_msg.AddInt64("scroll_rate", 500000 );
			Window()->PostMessage( &win_msg );
		}	break;
		case 'SCno':
		{ // Set scroll rate, NORMAL
			BMessage win_msg( PANEL_SET_SCROLL_RATE );
			win_msg.AddInt64("scroll_rate", 100000 );
			Window()->PostMessage( &win_msg );
		}	break;
		case 'SCfa':
		{ // Set scroll rate, FAST
			BMessage win_msg( PANEL_SET_SCROLL_RATE );
			win_msg.AddInt64("scroll_rate", 20000 );
			Window()->PostMessage( &win_msg );
		}	break;
		case 'SCin':
		{ // Set scroll rate, INSTANT
			BMessage win_msg( PANEL_SET_SCROLL_RATE );
			win_msg.AddInt64("scroll_rate", 0 );
			Window()->PostMessage( &win_msg );
		}	break;
		case 'Semo':
		{ // Set expand on mouse .. ON
			BMessage win_msg(PANEL_SET_EXPAND_ON_MOUSE_OVER);
			win_msg.AddBool("expand_on_mouse_over",true);
			Window()->PostMessage( &win_msg );
		}	break;
		case 'Semf':
		{ // Set expand on mouse .. OFF
			BMessage win_msg(PANEL_SET_EXPAND_ON_MOUSE_OVER);
			win_msg.AddBool("expand_on_mouse_over",false);
			Window()->PostMessage( &win_msg );
		}	break;
		case 'REsh':
		{ // Show replicants
			BDragger::ShowAllDraggers();
		}	break;
		case 'REhi':
		{ // Hide replicants
			BDragger::HideAllDraggers();
		}	break;
		case kMsgShutdownSystem:
		case kMsgRebootSystem:
		{
			bool reboot = (msg->what == kMsgRebootSystem);
			bool confirm;
			msg->FindBool("confirm", &confirm);

			BRoster roster;
			BRoster::Private rosterPrivate(roster);
			status_t error = rosterPrivate.ShutDown(reboot, confirm, true);

			break;
		}		
/*
		{ // forward to Roster
			BMessenger msgr("application/x-vnd.Be-ROST");
			msgr.SendMessage( msg );
		}
			break; 														
*/
		
		default:
			BView::MessageReceived(msg);
	}
}

void
HandleView::ShowPopup( BPoint pos )
{
	PanelWindow * panel = (PanelWindow*)Window();
	
	if (fMenu == NULL) {
		fMenu = new BPopUpMenu(
			"context",
			false, // radio
			false // label from marked
		);
	
		fIconSizeMenu = new BMenu( B_TRANSLATE(kIcoSize) );
		// create items
		fIconSizeMenu->AddItem( new BMenuItem(B_TRANSLATE(kLargeIco), new BMessage('Lico')) );
		fIconSizeMenu->AddItem( new BMenuItem(B_TRANSLATE(kSmallIco), new BMessage('Sico')) );
		// set target
		fIconSizeMenu->SetTargetForItems( this );
		fMenu->AddItem(fIconSizeMenu);

		fThicknessMenu = new BMenu( B_TRANSLATE(kThickness));
		// create items
		fThicknessMenu->AddItem( new BMenuItem(B_TRANSLATE(kThickNormal), new BMessage('THno')) );
		fThicknessMenu->AddItem( new BMenuItem(B_TRANSLATE(kThickThin), new BMessage('THth')) );
		// set target
		fThicknessMenu->SetTargetForItems( this );
		fMenu->AddItem(fThicknessMenu);

		fScrollSpeedMenu = new BMenu( B_TRANSLATE(kScrollspeed));
		fScrollSpeedMenu->AddItem( new BMenuItem(B_TRANSLATE(kSpeedSlow), new BMessage('SCsl')) );
		fScrollSpeedMenu->AddItem( new BMenuItem(B_TRANSLATE(kSpeedNormal), new BMessage('SCno')) );
		fScrollSpeedMenu->AddItem( new BMenuItem(B_TRANSLATE(kSpeedFast), new BMessage('SCfa')) );
		fScrollSpeedMenu->AddItem( new BMenuItem(B_TRANSLATE(kSpeedInstant), new BMessage('SCin')) );
		// set target
		fScrollSpeedMenu->SetTargetForItems( this );
		fMenu->AddItem(fScrollSpeedMenu);

		fExpandMenu = new BMenu( B_TRANSLATE(kExpOnMouseOver));
		fExpandMenu->AddItem( new BMenuItem(B_TRANSLATE(kOn), new BMessage('Semo')) );
		fExpandMenu->AddItem( new BMenuItem(B_TRANSLATE(kOff), new BMessage('Semf')) );
		// set target
		fExpandMenu->SetTargetForItems( this );
		fMenu->AddItem(fExpandMenu);

		fFloatMenu = new BMenu( B_TRANSLATE(kFloating));
		fFloatMenu->AddItem( new BMenuItem(B_TRANSLATE(kOn), new BMessage('Sfly')) );
		fFloatMenu->AddItem( new BMenuItem(B_TRANSLATE(kOff), new BMessage('Sfln')) );
		// set target
		fFloatMenu->SetTargetForItems( this );
		fMenu->AddItem(fFloatMenu);

		BMenu* sub_menu;
		sub_menu = new BMenu( B_TRANSLATE(kPanel));
		sub_menu->AddItem( new BMenuItem(B_TRANSLATE(kPanelNew), new BMessage('Npan')) );
		sub_menu->AddItem( new BMenuItem(B_TRANSLATE(kPanelRemove), new BMessage('Rpan')) );
		// set target
		sub_menu->SetTargetForItems( this );
		fMenu->AddItem( sub_menu );

		fMenu->AddSeparatorItem();

		fReplicantsMenu = new BMenu( B_TRANSLATE(kReplicants));
		fReplicantsMenu->AddItem( new BMenuItem(B_TRANSLATE(kRepShow), new BMessage('REsh')) );
		fReplicantsMenu->AddItem( new BMenuItem(B_TRANSLATE(kRepHide), new BMessage('REhi')) );
		// set target
		fReplicantsMenu->SetTargetForItems( this );
		fMenu->AddItem(fReplicantsMenu);

		sub_menu = new BMenu( B_TRANSLATE(kShutdown));
		sub_menu->AddItem( new BMenuItem(B_TRANSLATE(kShutdownReboot), new BMessage(kMsgRebootSystem)) );
		sub_menu->AddItem( new BMenuItem(B_TRANSLATE(kShutdownPoweroff), new BMessage(kMsgShutdownSystem)) );
		// set target
		sub_menu->SetTargetForItems( this );
		fMenu->AddItem( sub_menu );

		fMenu->AddSeparatorItem();

		fMenu->AddItem( fAutoStartMenu = new BMenuItem(B_TRANSLATE("Autostart"), new BMessage('Auto')) );
		fMenu->AddItem( new BMenuItem(B_TRANSLATE(kQuit), new BMessage('Quit')) );

		fMenu->SetTargetForItems( this );
	}
	
	// check if large
	bool large_icons = false;
	if ( Bounds().Width() > Bounds().Height() )
	{
		if ( Bounds().Width() > 20 )
			large_icons = true;
	} else
	{
		if ( Bounds().Height() > 20 )
			large_icons = true;
	}
	// set mark
	fIconSizeMenu->FindItem(B_TRANSLATE(kLargeIco))->SetMarked(large_icons);
	fIconSizeMenu->FindItem(B_TRANSLATE(kSmallIco))->SetMarked(!large_icons);
	
	// check if thick
	bool thin_handle = false;
	if ( Bounds().Width() == 1 || Bounds().Height() == 1 )
	{
		thin_handle = true;
	}
	// set mark
	fThicknessMenu->FindItem(B_TRANSLATE(kThickNormal))->SetMarked(thin_handle);
	fThicknessMenu->FindItem(B_TRANSLATE(kThickThin))->SetMarked(!thin_handle);

	// check mark
	int delay = panel->AnimationDelay();
	fScrollSpeedMenu->FindItem(B_TRANSLATE(kSpeedSlow))->SetMarked(delay == 500000);
	fScrollSpeedMenu->FindItem(B_TRANSLATE(kSpeedNormal))->SetMarked(delay == 100000);
	fScrollSpeedMenu->FindItem(B_TRANSLATE(kSpeedFast))->SetMarked(delay == 20000);
	fScrollSpeedMenu->FindItem(B_TRANSLATE(kSpeedInstant))->SetMarked(delay == 0);

	// set mark
	bool floating =  Window()->Feel() == B_FLOATING_ALL_WINDOW_FEEL;
	fFloatMenu->FindItem(B_TRANSLATE(kOn))->SetMarked(floating);
	fFloatMenu->FindItem(B_TRANSLATE(kOff))->SetMarked(!floating);

	// set mark
	bool expand = panel->IsExpandOnMouseOver();
	fExpandMenu->FindItem(B_TRANSLATE(kOn))->SetMarked(expand);
	fExpandMenu->FindItem(B_TRANSLATE(kOff))->SetMarked(!expand);

	// set mark
	bool dragger = BDragger::AreDraggersDrawn();
	fReplicantsMenu->FindItem(B_TRANSLATE(kRepShow))->SetMarked(dragger);
	fReplicantsMenu->FindItem(B_TRANSLATE(kRepHide))->SetMarked(!dragger);

	fMenu->Go(
			pos,
			true,	// delivers msg
			true	// open anyway
			);
}

#include "IconView.h"
#include "PanelWindow.h"
#include "GetStringWindow.h"
#include "BubbleWindow.h"

#include <stdio.h>
#include <Node.h>
#include <NodeInfo.h>
#include <Window.h>
#include <PopUpMenu.h>
#include <MenuItem.h>
#include <Roster.h>
#include <AppFileInfo.h>

#include <Catalog.h>
#include <Locale.h>

#undef B_TRANSLATION_CONTEXT
#define B_TRANSLATION_CONTEXT "LnLauncherContextMenu"

void icon_got_string( void * _icon, const char * text )
{
	IconView * icon = (IconView*)_icon;
	
	icon->Run( text );
}

IconView::IconView( entry_ref * ref )
:	BView(  
		BRect(0,0,35,35), "icon", B_FOLLOW_NONE, B_WILL_DRAW
	),
	m_path( ref ), m_actual_path( ref ),
	m_large_icon( new BBitmap(BRect(0,0,31,31), B_RGB32) ), // changed B_CMAP8 to B_RGB32
	m_small_icon( new BBitmap(BRect(0,0,15,15), B_RGB32) ), // changed B_CMAP8 to B_RGB32
	m_mouse_down( false ),
	m_moving( false )
{
	SetViewColor(ui_color(B_PANEL_BACKGROUND_COLOR));
	
	Update();
}

IconView::IconView( BMessage * msg )
:	BView( msg ),
	m_large_icon( new BBitmap(BRect(0,0,31,31), B_RGB32) ), // changed B_CMAP8 to B_RGB32
	m_small_icon( new BBitmap(BRect(0,0,15,15), B_RGB32) ), // changed B_CMAP8 to B_RGB32
	m_mouse_down( false ),
	m_moving( false ),
	m_buttons( 0 )
{
	m_app_sig.SetTo( msg->FindString("app_sig") );
	m_path = msg->FindString("path");
	m_actual_path = m_path;
	
	m_args_string = msg->FindString("args_string");
	
	Update();
}

IconView::~IconView()
{
	if ( m_large_icon )
		delete m_large_icon;
	if ( m_small_icon )
		delete m_small_icon;
}

BArchivable *
IconView::Instantiate( BMessage * archive )
{
	if ( !validate_instantiation(archive, "IconView") ) 
		return NULL;
	return new IconView(archive);
}

status_t
IconView::Archive( BMessage * msg, bool deep ) const
{
	msg->AddString("path", m_path.Path() );
	if ( m_app_sig.InitCheck() == B_OK )
		msg->AddString("app_sig", m_app_sig.Type() );
//	else
//		msg->AddString("app_sig", "" );
	msg->AddString("args_string", m_args_string.String() );
	
	return BView::Archive( msg, deep );
}

void
IconView::DetachedFromWindow()
{
	m_mouse_down = false;
}

void
IconView::Draw( BRect )
{
	rgb_color light  = ViewColor();
	rgb_color bright = ViewColor();
	rgb_color dark   = ViewColor();
	
	// set up colors
	if ( bright.red < 175 )   bright.red += 80;   else bright.red = 255;
	if ( bright.green < 175 ) bright.green += 80; else bright.green = 255;
	if ( bright.blue < 175 )  bright.blue += 80;  else bright.blue = 255;
	
	if ( dark.red > 79 )   dark.red -= 80;   else dark.red = 0;
	if ( dark.green > 79 ) dark.green -= 80; else dark.green = 0;
	if ( dark.blue > 79 )  dark.blue -= 80;  else dark.blue = 0;
	
	if ( m_mouse_down )
	{
		light	= tint_color( light, B_HIGHLIGHT_BACKGROUND_TINT );
		bright	= tint_color( bright, B_HIGHLIGHT_BACKGROUND_TINT );
		dark	= tint_color( dark, B_HIGHLIGHT_BACKGROUND_TINT );
	}
	
	BRect bounds = Bounds();
	
	// border
	MovePenTo( bounds.RightTop() );
	
	SetHighColor( bright );		
	StrokeLine( bounds.LeftTop() );
	StrokeLine( bounds.LeftBottom() );
	
	SetHighColor( dark );
	StrokeLine( bounds.RightBottom() );
	StrokeLine( bounds.RightTop() );
	
	// interior
	BRect temp = bounds; temp.InsetBy(1,1);
	SetHighColor( light );		FillRect( temp );
	
	if (m_large_icon && bounds.Width() > 30)
	{
		SetBlendingMode( B_PIXEL_ALPHA, B_ALPHA_OVERLAY );
		SetDrawingMode( B_OP_ALPHA );
		DrawBitmap( m_large_icon, BPoint(2,2) );
	} else
	if (m_small_icon)
	{
		SetBlendingMode( B_PIXEL_ALPHA, B_ALPHA_OVERLAY );
		SetDrawingMode( B_OP_ALPHA );
		DrawBitmap( m_small_icon, BPoint(2,2) );
	}
}

void
IconView::MouseDown( BPoint )
{
	SetMouseEventMask( B_POINTER_EVENTS, B_LOCK_WINDOW_FOCUS );
	Window()->CurrentMessage()->FindInt32("buttons",&m_buttons);
	
	m_mouse_down = true;
	
	Invalidate();
}

void
IconView::MouseMoved( BPoint pos, uint32 transit, const BMessage * )
{
	BPoint scr_pos = ConvertToScreen( pos );
	
	// for 'expand on mouse over'
	if ( transit == B_ENTERED_VIEW )
	{ // enter view
		Window()->PostMessage( PANEL_MOUSE_ENTER );
	}
	
	if ( transit == B_EXITED_VIEW || transit == B_OUTSIDE_VIEW )
	{ // exit view (and window)
		if ( !Window()->Frame().Contains( scr_pos ) )
			Window()->PostMessage( PANEL_MOUSE_EXIT );
	}
	
	if ( 
		m_mouse_down && 
		(m_buttons & B_PRIMARY_MOUSE_BUTTON) && 
		!Bounds().Contains(pos)
	)
	{ // outside of view, begin drag&drop
		BBitmap * bmp = m_large_icon ? m_large_icon : m_small_icon;
		
		BMessage msg('DATA');
		
		// get entry ref for msg
		BEntry entry( m_path.Path(), false /* don't traverse */ );
		entry_ref ref;
		entry.GetRef( &ref );
		
		msg.AddRef( "refs", &ref );
		
		msg.AddPointer("panel",Window());
		
		if ( !bmp )
			DragMessage( &msg, Bounds() );
		else
		{
			// copy bitmap
			BMessage bitmap;
			bmp->Archive( &bitmap );
			bmp = new BBitmap( &bitmap );
			
			// drag it
			DragMessage( 
				&msg, bmp, B_OP_ALPHA, 
				BPoint(bmp->Bounds().right/2, bmp->Bounds().bottom/2)
			);
		}
		
		m_moving = false;
		m_mouse_down = false;
		
		Invalidate();
	} else
	if (
		m_mouse_down && 
		(m_buttons & B_SECONDARY_MOUSE_BUTTON) && 
		!Bounds().Contains( pos ) && 
		Window()->Frame().Contains( scr_pos )
	)
	{ // secondary button, outside view, in window: move icon
		m_moving = true;
		
		PanelWindow::ScreenEdge edge = PanelWindow::LEFT;
		
		if ( pos.y < 0 ) // above
			edge = PanelWindow::TOP;
		if ( pos.x < 0 ) // left
			edge = PanelWindow::LEFT;
		if ( pos.y > Bounds().bottom ) // below
			edge = PanelWindow::BOTTOM;
		if ( pos.x > Bounds().right ) // right
			edge = PanelWindow::RIGHT;
		
		BMessage msg( PANEL_MOVE_ICON );
		msg.AddPointer( "icon", this );
		msg.AddInt8("edge",edge);
		
		Window()->PostMessage( &msg );
	}
}

void 
IconView::MouseUp( BPoint pos )
{
	if ( 
		m_mouse_down && 
		!m_moving && 
		(m_buttons & B_SECONDARY_MOUSE_BUTTON)
	)
	{ // context menu
		BPath parent;
		m_actual_path.GetParent( &parent );
		BEntry entry( parent.Path() );
		
		if ( !entry.Exists() )
		{
			Update( false );
			m_actual_path.GetParent( &parent );
			entry.SetTo( parent.Path() );
		}
		
		BPopUpMenu * menu = new BPopUpMenu(
			"",
			false, // radio
			false // label from marked
		);
		
		if ( entry.Exists() )
		{ // only display if file exists
			menu->AddItem( new BMenuItem(B_TRANSLATE("Run with args"), new BMessage('Ruar')) );
			menu->AddItem( new BMenuItem(B_TRANSLATE("Open parent"), new BMessage('Opar')) );
			menu->AddSeparatorItem();
		}
		menu->AddItem( new BMenuItem(B_TRANSLATE("Remove"), new BMessage('Remo')) );
		
		menu->SetTargetForItems( this );
		
		menu->Go(
			ConvertToScreen( pos ),
			true,	// delivers msg
			true	// open anyway
		);
		
		delete menu;
	} else
	if (
		m_mouse_down &&
		!m_moving &&
		(m_buttons & B_PRIMARY_MOUSE_BUTTON) &&
		Bounds().Contains( pos )
	)
	{ // clicked
		Run();
	}
	
	m_mouse_down = false;
	m_moving = false;
	
	Invalidate();
}

void
IconView::MessageReceived( BMessage * msg )
{
	switch ( msg->what )
	{
		case 'DATA':
		{ // file drop
			int32 buttons = 0;
			msg->FindInt32("buttons",&buttons);
			
			if ( buttons & B_SECONDARY_MOUSE_BUTTON )
			{ // open with
				
			} else
			{ // add icon
				msg->what = PANEL_ADD_ICON;
				Window()->PostMessage(msg);
			}
		}	break;
		case 'Remo':
		{ // remove icon
			BMessage a_msg( PANEL_REMOVE_ICON );
			a_msg.AddPointer("icon", this);
			Window()->PostMessage( &a_msg );
		}	break;
		case 'PSTE':
		{ // color drop
			rgb_color * col;
			ssize_t sz;
			if (msg->FindData("RGBColor",B_RGB_COLOR_TYPE, (const void**)&col, &sz) == B_OK)
			{
				SetViewColor( *col );
				Invalidate();
			}
		}	break;
		case 'Opar':
		{
			OpenParent();
		}	break;
		case 'Ruar':
		{ // run with arguments
			GetStringWindow * win = new GetStringWindow(
				icon_got_string,
				this,
				("Args"),
				m_args_string.String()
			);
			win->Show();
		}	break;
		case GET_STRING_WINDOW_RESULT:
		{ // got string from args-window, Run!
			const char * arg_str = msg->FindString("string");
			Run( arg_str );
		}	break;
		case BUBBLE_TEXT_REQUEST:
		{
			BMessage reply( BUBBLE_TEXT_REPLY );
			reply.AddString("text", m_path.Leaf() );
			msg->SendReply( &reply );
		}	break;
		default:
			BView::MessageReceived(msg);
	}
}

void
IconView::Run( const char * args )
{
	BEntry entry( m_actual_path.Path(), true );
	
	BNode node( &entry );
	
	if ( node.InitCheck() != B_OK )
	{ // file not found
		Update( false );
		
		entry.SetTo( m_actual_path.Path() );
		
		node.SetTo( &entry );
	}
	
	if ( node.InitCheck() != B_OK )
	{ // file doesn't exist
		return;
	}
	
	entry_ref ref;
	entry.GetRef( &ref );
	
	BNodeInfo node_info( &node );
	char mime[B_MIME_TYPE_LENGTH+1];
	node_info.GetType( mime );
	
	BMimeType mime_type( mime );
	
	BMessage msg( B_REFS_RECEIVED );
	msg.AddRef("refs",&ref);
	
	if ( mime_type == "application/x-vnd.Be-directory" )
	{ // folder, open with preferred app
		be_roster->Launch( mime_type.Type(), &msg );
		return;
	}
	
	if ( args )
	{ // extract args
		m_args_string = args;
		
		char * argv[256];
		int num_arg = 0;
		/*
			convert single string to array of strings
		*/
		const char * curr = args;
		const char * start = NULL;
		while ( *curr )
		{
			while ( *curr == ' ' ) curr++;
			start = curr;
			
			if ( *curr == '"' )
			{ // multi-word arg
				curr++; start++;
				while ( *curr && *curr != '"' ) curr++;
			} else {
				while ( *curr && *curr != ' ' ) curr++;
			}
			
			ptrdiff_t len = curr - start;
			
			if ( *curr == '"' ) curr++; // skip '"' if needed
			
			argv[num_arg] = new char[ len + 1 ];
			strncpy(argv[num_arg], start, len );
			argv[num_arg][len] = 0;
			
			num_arg++;
		}
		
		be_roster->Launch( &ref, num_arg, argv );
		
		for ( int c=0; c<num_arg; c++ )
			delete [] argv[c];
		
		return;
	}

	be_roster->Launch( &ref );
}

void
IconView::Update( bool get_icons )
{
	// find file
	BEntry entry( m_path.Path(), true );
	BNode node( &entry );
	
	if ( (node.InitCheck() != B_OK) && (m_app_sig.InitCheck() == B_OK))
	{ // file not found, mime type present, look for app
		BMimeType super_type;
		
		m_app_sig.GetSupertype( &super_type );
		
		if ( super_type == "application" )
		{
			entry_ref ref;
			if ( be_roster->FindApp( m_app_sig.Type(), &ref ) == B_OK )
			{
				m_actual_path.SetTo( &ref );
				node.SetTo( m_actual_path.Path() );
			}
		}
	} else
	{
		m_actual_path = m_path;
	}
	
	bool found_large = false, found_small = false;
	
	if ( node.InitCheck() == B_OK )
	{
		BNodeInfo n_info( &node );
		
		if ( m_app_sig.InitCheck() != B_OK )
		{
			// get mime type
			char type[B_MIME_TYPE_LENGTH+1];
			n_info.GetType( type );
			BMimeType mime( type );
		
			if ( mime == "application/x-vnd.Be-elfexecutable" )
			{ // application, get app_sig
				BFile file( m_path.Path(), B_READ_ONLY );
				BAppFileInfo info( &file );
				
				char mime[B_MIME_TYPE_LENGTH];
				if ( info.GetSignature( mime ) == B_OK )
					m_app_sig.SetTo( mime );
			}
		}
		
		if ( get_icons )
		{
			// large icon
			if ( n_info.GetTrackerIcon( m_large_icon, B_LARGE_ICON ) == B_OK )
			{
				found_large = true;
			}
			// small icon
			if ( n_info.GetTrackerIcon( m_small_icon, B_MINI_ICON ) == B_OK )
			{
				found_small = true;
			}
		}
	}
	
	if ( get_icons )
	{
		// if not found, use broken link icon
		BMimeType mt("application/x-vnd.Be-symlink");
		
		if (!found_large)
			if ( mt.GetIcon( m_large_icon, B_LARGE_ICON ) != B_OK )
				{ delete m_large_icon; m_large_icon = NULL; }
		if (!found_small)
			if ( mt.GetIcon( m_small_icon, B_MINI_ICON ) != B_OK )
				{ delete m_small_icon; m_small_icon = NULL; }
	}
}

void
IconView::OpenParent()
{
	BPath parent;
	m_actual_path.GetParent( &parent );
	
	BEntry entry( parent.Path() );
	
	if ( !entry.Exists() )
	{ // file not found
		return;
	}
	
	entry_ref ref;
	if ( entry.GetRef( &ref ) != B_OK )
		printf("get ref fail\n");
		
	BMessage msg( B_REFS_RECEIVED );
	msg.AddRef("refs",&ref);

	status_t res = be_roster->Launch( "application/x-vnd.Be-directory", &msg );
	
	if ( res != B_OK && res != B_ALREADY_RUNNING)
	{ // didn't open, ask Tracker if already open and bring to front if so
		printf("open parent fail [%s]\n", parent.Path() );
	}
}

